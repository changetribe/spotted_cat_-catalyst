##Additional code comparing randomForest to other classification algorithms
install.packages("MASS")
library(MASS)
install.packages("randomForest")
library(randomForest)
install.packages("ROCR")
library(ROCR)
install.packages("nnet")
library(nnet)
install.packages("rpart")
library(rpart)

# SET WORKING DIRECTORY (If pasting from windows explorer, you must change the "\" to "/"
# This is the location of the results from the SpottedCatalyst from Fiji and either the saved models or the training data
setwd("C:/Users/SCKEFAUVER/Google Drive/SpottedCats/GOLD/SpottedCatalyst_withColor")
list.files() #This should include at least: SpottedCatalystTrainingData.csv, results.xls, GOLDtestSetMeta.csv

mydata.train <- read.table("results_train_with_color.xls", header=TRUE, sep="\t")
names(mydata.train)[names(mydata.train)=="Slice"] <- "ImageID"
names(mydata.train)
str(mydata.train)
# Clean up the data - remove book keeping and buggy columns
mydata.train$Mean <- NULL
mydata.train$Mode <- NULL
mydata.train$Median <- NULL
mydata.train$Slice2 <- NULL
mydata.train$Mean2 <- NULL
mydata.train$Mode2 <- NULL
mydata.train$Median2 <- NULL
names(mydata.train)

mydata.qcmeta <- read.table("spottedcatmeta_originalqc25k.csv", header=TRUE, sep=";")
names(mydata.qcmeta)

data.train <- merge(mydata.train, mydata.qcmeta, by="ImageID")
names(mydata.train)
write.table(data.train, "SpottedCatalystTrainingData_withColor.csv", row.names=FALSE, sep=",")

# LOAD IN THE FULL TRAINING DATA SET FOR PROCESSING
## data.train <- read.table("SpottedCatalystTrainingData_withColor.csv", header=TRUE, sep=",")
# Check the results with the original
##names(data.train)

# Remove the Unique Identifier from the Training Data for Testing Purposes
data.train$ImageID <-NULL
names(data.train)

# LOAD IN THE RESULTS AND MERGE WITH THE META TO MAKE THE VALIDATION/TESTING DATA SET
mydata.results <- read.table("results_GOLD_with_color.xls", header=TRUE, sep="\t")
# Double check the header names to make sure your separator is correct!
names(mydata.results)
str(mydata.results)
# Clean up the results file - remove book keeping and buggy columns
names(mydata.results)[names(mydata.results)=="Slice"] <- "ImageID"
mydata.results$Mean <- NULL
mydata.results$Mode <- NULL
mydata.results$Median <- NULL
mydata.results$Slice2 <- NULL
mydata.results$Mean2 <- NULL
mydata.results$Mode2 <- NULL
mydata.results$Median2 <- NULL
names(mydata.results)

# READ IN THE METADATA WITH THE VALIDATION INFORMATION
catmetaval <- read.table("GOLDtestSetMeta.csv", header=TRUE, sep=",")
# Check that the headers came in correctly and make sure the headers match
names(catmetaval)

# MERGE THE DATASETS
data.val <- merge(mydata.results, catmetaval, by="ImageID")
# Check the results with the original
names(data.val)
names(data.val)[names(data.val)=="target"] <- "spottedcat"
names(data.val)

# Save the merged results to file
write.table(data.val, "data.val_with_color.xls", row.names=FALSE, sep="\t")

#Comparison of final prepared training and validation data sets
names(data.train)
names(data.val)

## Rough fix the data quickly for NA and NaN values
data.train.na <- na.roughfix(data.train)
data.val.na <- na.roughfix(data.val)

## RUN A COMPARISON OF DIFFERENT MODELS, USING THE RFC3 VERSION OF RANDOM FOREST

# Logistic Regression
logit.fit = glm(spottedcat ~., family = binomial(logit),data = data.train.na)
logit.fit
summary(logit.fit)
#anova(logit.fit)
#plot(logit.fit)

install.packages("MASS")
library(MASS)
# Stepwise improve the GLM based on AIC this may take a long time...
logit.fitAIC = stepAIC(logit.fit) 
logit.fitAIC
summary(logit.fitAIC) 
#anova(logit.fitAIC)
#plot(logit.fitAIC)
coef(logit.fitAIC)

logit.preds = predict(logit.fitAIC,newdata=data.val.na,type="response")
logit.pred = prediction(logit.preds,data.val$spottedcat)
logit.perf = performance(logit.pred,"tpr","fpr")

# Random Forest
rf.fit <-randomForest(as.factor(spottedcat)~.,data=data.train.na, ntree=1001,
                        keep.forest=TRUE,importance=TRUE,sampsize=c(100, 300),test=data.val.na)
rf.preds = predict(rf.fit,type="prob",newdata=data.val.na)[,2]
rf.pred = prediction(rf.preds, data.val.na$spottedcat)
rf.perf = performance(rf.pred,"tpr","fpr")

# CART Trees
mycontrol = rpart.control(cp = 0, xval = 10)
tree.fit = rpart(as.factor(spottedcat)~., method = "class", data = data.train.na, control = mycontrol)
tree.fit$cptable
tree.prune = prune(tree.fit,cp=tree.fit$cptable[which.min(tree.fit$cptable[,"xerror"]),"CP"])
tree.preds = predict(tree.prune,type="prob",newdata=data.val.na)[,2]
tree.pred = prediction(tree.preds,data.val.na$spottedcat)
tree.perf = performance(tree.pred,"tpr","fpr")

# Neural Network
nnet.fit = nnet(spottedcat~., data=data.train,size=20,maxit=10000,decay=.001)
nnet.preds = predict(nnet.fit,newdata=data.val,type="raw")
nnet.pred = prediction(nnet.preds,data.val$spottedcat)
nnet.perf = performance(nnet.pred,"tpr","fpr")

# Plotting ROC Curves
plot(logit.perf,col=2,lwd=2,main="ROC Curve for Different Classifiers on Gold Dataset")
plot(rf.perf,col=3,lwd=2,add=T)
plot(tree.perf,lwd=2,col=4,add=T)
plot(nnet.perf,lwd=2,col=5,add=T)
abline(a=0,b=1,lwd=2,lty=2,col="gray")
legend("bottomright",col=c(2:5),lwd=2,legend=c("logit","RF","CART","Neural Net"),bty='n')

# Save the ROC Curves to file
png(filename="ROC Curve for Different Classifiers on Gold Dataset.png")
plot(logit.perf,col=2,lwd=2,main="ROC Curve for Different Classifiers on Gold Dataset")
plot(rf.perf,col=3,lwd=2,add=T)
plot(tree.perf,lwd=2,col=4,add=T)
plot(nnet.perf,lwd=2,col=5,add=T)
abline(a=0,b=1,lwd=2,lty=2,col="gray")
legend("bottomright",col=c(2:5),lwd=2,legend=c("logit","RF","CART","Neural Net"),bty='n')
dev.off()



