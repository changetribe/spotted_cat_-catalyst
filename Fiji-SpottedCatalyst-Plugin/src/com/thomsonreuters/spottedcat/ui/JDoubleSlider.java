package com.thomsonreuters.spottedcat.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * 
 * @author Georges El-Haddad (georges.haddad@thomsonreuters.com)<br>
 * Created on: 2:49:04 PM - Aug 27, 2014<br>
 * <br>
 * Because Java does not provide a JSlider class that can contain floating
 * point values and that can display "Infinity" when it reaches a certain
 * maximum value.
 */
public class JDoubleSlider extends JPanel {
        
        private static final long serialVersionUID = 4412520642393800351L;
        
        private final DecimalFormat df = new DecimalFormat("0.00#");
        private final JTextField textField = new JTextField(5);
        private final JLabel label = new JLabel();
        private DoubleJSlider slider = null;
        private boolean showInfinityOnMax = false;
        
        public JDoubleSlider(int min, int max, int value, int scale) {
                slider = new DoubleJSlider(min, max, value, scale);
                slider.addChangeListener(new ChangeListener(){
                        @Override
                        public void stateChanged(ChangeEvent e) {
                                setValue(slider.getScaledValue());
                        }
                });
                
                textField.addKeyListener(new KeyAdapter(){
                        @Override
                        public void keyReleased(KeyEvent ke) {
                                String typed = textField.getText();
                                slider.setValue(0);
                                if(typed.equalsIgnoreCase("infinity")) {
                                        slider.setValue(slider.getMaximum());
                                        return;
                                }
                                
                                if(!typed.matches("\\d+(\\.\\d*)?")) {
                                        return;
                                }
                                
                                double value = Double.parseDouble(typed)*slider.scale;
                                slider.setValue((int)value);
                        }
                });
                
                setValue(slider.getScaledValue());
                
                setLayout(new BorderLayout(5, 5));
                add(slider, BorderLayout.CENTER);
                add(textField, BorderLayout.EAST);
                add(label, BorderLayout.WEST);
        }
        
        private final void setValue(double scaledValue) {
                if (scaledValue == (slider.getMaximum() / slider.scale)) {
                        if (showInfinityOnMax) {
                                textField.setText("Infinity");
                        }
                        else {
                                textField.setText(df.format(slider.getScaledValue()));
                        }
                }
                else {
                        textField.setText(df.format(slider.getScaledValue()));
                }
        }
        
        public final void setLabel(String text) {
                label.setText(text);
        }
        
        public final void setLabel(String text, Dimension preferredSize) {
                label.setText(text);
                label.setPreferredSize(preferredSize);
        }
        
        public final void setShowInfinityOnMax(boolean showInfinityOnMax) {
                this.showInfinityOnMax = showInfinityOnMax;
                setValue(slider.getScaledValue());
        }
        
        public final boolean isShowInfinityOnMax() {
                return showInfinityOnMax;
        }
        
        public final double getValue() {
                return slider.getScaledValue();
        }
}

/**
 * 
 * @author Georges El-Haddad (georges.haddad@thomsonreuters.com)<br>
 * Created on: 2:48:48 PM - Aug 27, 2014
 */
class DoubleJSlider extends JSlider {
        
        private static final long serialVersionUID = 6849724423814902356L;
        protected final int scale;

        DoubleJSlider(int min, int max, int value, int scale) {
                super(min, max, value);
                this.scale = scale;
        }

        double getScaledValue() {
                return ((double)super.getValue()) / this.scale;
        }
        
        int getScale() {
                return scale;
        }
}
