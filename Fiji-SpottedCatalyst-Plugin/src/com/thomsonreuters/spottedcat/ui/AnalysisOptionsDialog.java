package com.thomsonreuters.spottedcat.ui;
import ij.IJ;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

/**
 * 
 * @author Georges El-Haddad (georges.haddad@thomsonreuters.com)<br>
 * Created on: 4:50:13 PM - Aug 22, 2014
 * <p>
 * This class is a modal dialog representing a list of pre-procssing
 * options for the user the select before starting the batch process
 * of image analysis. The decision to write an options dialog from
 * scratch was that the pre-made ones from ImageJ were tedious to work
 * with and used only AWT objects which were heavier and less versitile
 * than Swing. Here we can create more complex and robust options to
 * present to the user.
 */
public class AnalysisOptionsDialog extends JDialog {

        private static final long serialVersionUID = 2388363240216989513L;

        private AnalysisOptions analysisOpt = null;

        private String inputDirPath = null;
        private String metadataPath = null;
        private JButton okButton = null;
        private JButton cancelButton = null;

        private JSpinner subBackgroundSpinner = new JSpinner(new SpinnerNumberModel(50.0, 0.0, 90.0, 0.01));
        private JSpinner subBackgroundDaySpinner = new JSpinner(new SpinnerNumberModel(50.0, 0.0, 90.0, 0.01));
        private JSpinner subBackgroundNightSpinner = new JSpinner(new SpinnerNumberModel(25.0, 0.0, 90.0, 0.01));
        private JCheckBox subBackgroundCheckBox = new JCheckBox("Use Day/Night ?", true);
        private JCheckBox subBackgroundLightCheckBox = new JCheckBox("Light Background", false);
        private JDoubleSlider minParticleSizeSlider = new JDoubleSlider(0, 9999, 8, 1);
        private JDoubleSlider maxParticleSizeSlider = new JDoubleSlider(0, 100, 100, 100);
        private JDoubleSlider minParticleCircSlider = new JDoubleSlider(0, 100, 60, 100);
        private JDoubleSlider maxParticleCircSlider = new JDoubleSlider(0, 100, 100, 100);
        private JComboBox thresholdComboBox = new JComboBox(new String[] { "Default", "Huang", "Intermodes", "IsoData", "IJ_IsoData", "Li", "MaxEntropy", "Mean", "MinError", "Minimum", "Moments", "Otsu", "Percentile", "RenyiEntropy", "Shanbhag", "Triangle", "Yen"});
        private JCheckBox thresholdDarkBbgCheckBox = new JCheckBox("Dark Background", false);
        private JSpinner blockRadiusSpinner = new JSpinner(new SpinnerNumberModel(127, 0, 255, 1));
        private JSpinner binsSpinner = new JSpinner(new SpinnerNumberModel(256, 0, 999, 1));
        private JSpinner slopeSpinner = new JSpinner(new SpinnerNumberModel(3.0, 0.0, 100.0, 0.01));
        private JSpinner slopeDaySpinner = new JSpinner(new SpinnerNumberModel(3.0, 0.0, 100.0, 0.01));
        private JSpinner slopeNightSpinner = new JSpinner(new SpinnerNumberModel(8.0, 0.0, 100.0, 0.01));
        private JCheckBox metaDataCheckBox = new JCheckBox("Use Metadata CSV ?", false);
        private JCheckBox saveSummaryCheckBox = new JCheckBox("Save CSV Summaries to file", true);
        private JCheckBox saveOverlaysCheckBox = new JCheckBox("Save Analyzed Overlays", false);
        private JCheckBox slopeDayNightCheckBox = new JCheckBox("Use Day/Night ?", true);
        private JCheckBox twoPassCheckBox = new JCheckBox("Make 2 Analysis Passes", false);
        private JDoubleSlider twoPassMinParticleSizeSlider = new JDoubleSlider(0, 9999, 25, 1);
        private JDoubleSlider twoPassMaxParticleSizeSlider = new JDoubleSlider(0, 100, 100, 100);
        private JDoubleSlider twoPassMinParticleCircSlider = new JDoubleSlider(0, 100, 40, 100);
        private JDoubleSlider twoPassMaxParticleCircSlider = new JDoubleSlider(0, 100, 100, 100);
        
        public AnalysisOptionsDialog() {
                subBackgroundSpinner.setEnabled(false);
                subBackgroundSpinner.setPreferredSize(new Dimension(60, 20));
                subBackgroundDaySpinner.setPreferredSize(new Dimension(60, 20));
                subBackgroundNightSpinner.setPreferredSize(new Dimension(60, 20));
                subBackgroundCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                if (((JCheckBox) e.getSource()).isSelected()) {
                                        subBackgroundSpinner.setEnabled(false);
                                        subBackgroundDaySpinner.setEnabled(true);
                                        subBackgroundNightSpinner.setEnabled(true);
                                }
                                else {
                                        subBackgroundSpinner.setEnabled(true);
                                        subBackgroundDaySpinner.setEnabled(false);
                                        subBackgroundNightSpinner.setEnabled(false);
                                }
                        }
                });

                JLabel subBackgroundLabel = new JLabel("Subtract Background:");
                JLabel subBackgroundDayLabel = new JLabel("Subtract Background (Day):");
                JLabel subBackgroundNightLabel = new JLabel("Subtract Background (Night):");

                subBackgroundLabel.setPreferredSize(new Dimension(165, 20));
                subBackgroundDayLabel.setPreferredSize(new Dimension(165, 20));
                subBackgroundNightLabel.setPreferredSize(new Dimension(165, 20));

                JPanel subBackgroundPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
                subBackgroundPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                subBackgroundPanel.add(subBackgroundLabel);
                subBackgroundPanel.add(subBackgroundSpinner);
                subBackgroundPanel.add(subBackgroundLightCheckBox);
                subBackgroundPanel.add(subBackgroundCheckBox);

                JPanel subBackgroundDayPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
                subBackgroundDayPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                subBackgroundDayPanel.add(subBackgroundDayLabel);
                subBackgroundDayPanel.add(subBackgroundDaySpinner);

                JPanel subBackgroundNightPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
                subBackgroundNightPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                subBackgroundNightPanel.add(subBackgroundNightLabel);
                subBackgroundNightPanel.add(subBackgroundNightSpinner);

                Box subBackgroundBox = Box.createVerticalBox();
                subBackgroundBox.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
                subBackgroundBox.add(subBackgroundPanel);
                subBackgroundBox.add(subBackgroundDayPanel);
                subBackgroundBox.add(subBackgroundNightPanel);

                // ------------------------------------------------------------
                
                minParticleSizeSlider.setLabel("Min Particle Size (px):");
                maxParticleSizeSlider.setLabel("Max:", new Dimension(123, 20));
                maxParticleSizeSlider.setShowInfinityOnMax(true);
                
                JPanel minParticleSizePanel = new JPanel(new GridLayout(2, 1, 4, 4));
                minParticleSizePanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
                minParticleSizePanel.add(minParticleSizeSlider);
                minParticleSizePanel.add(maxParticleSizeSlider);

                // --------------------------------------------------------

                minParticleCircSlider.setLabel("Min Particle Circularity:");
                maxParticleCircSlider.setLabel("Max:", new Dimension(131, 20));

                JPanel minParticleCircPanel = new JPanel(new GridLayout(2, 1, 4, 4));
                minParticleCircPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
                minParticleCircPanel.add(minParticleCircSlider);
                minParticleCircPanel.add(maxParticleCircSlider);

                // --------------------------------------------------------

                thresholdComboBox.setSelectedItem("Default");

                JLabel thresholdLabel = new JLabel("Auto Threshold Method:");
                thresholdLabel.setPreferredSize(new Dimension(165, 20));

                JPanel thresholdPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
                thresholdPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
                thresholdPanel.add(thresholdLabel);
                thresholdPanel.add(thresholdComboBox);
                thresholdPanel.add(thresholdDarkBbgCheckBox);

                // --------------------------------------------------------

                blockRadiusSpinner.setPreferredSize(new Dimension(60, 20));
                blockRadiusSpinner.setToolTipText("Block Size");

                binsSpinner.setPreferredSize(new Dimension(60, 20));
                binsSpinner.setToolTipText("Histogram Bins");

                slopeSpinner.setPreferredSize(new Dimension(60, 20));
                slopeSpinner.setToolTipText("Maximum Slope");
                slopeSpinner.setEnabled(false);

                slopeDaySpinner.setPreferredSize(new Dimension(60, 20));
                slopeDaySpinner.setToolTipText("Maximum Slope");

                slopeNightSpinner.setPreferredSize(new Dimension(60, 20));
                slopeNightSpinner.setToolTipText("Maximum Slope");

                final JPanel claheDayNightPanel = new JPanel(new GridLayout(2, 1, 0, 0));
                claheDayNightPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

                slopeDayNightCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                if (((JCheckBox) e.getSource()).isSelected()) {
                                        slopeSpinner.setEnabled(false);
                                        slopeDaySpinner.setEnabled(true);
                                        slopeNightSpinner.setEnabled(true);
                                }
                                else {
                                        slopeSpinner.setEnabled(true);
                                        slopeDaySpinner.setEnabled(false);
                                        slopeNightSpinner.setEnabled(false);
                                }
                        }
                });

                JLabel l1 = new JLabel("Max Slope (Day):");
                l1.setPreferredSize(new Dimension(165, 20));

                JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
                p1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                p1.add(l1);
                p1.add(slopeDaySpinner);

                JLabel l2 = new JLabel("Max Slope (Night):");
                l2.setPreferredSize(new Dimension(165, 20));

                JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
                p2.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                p2.add(l2);
                p2.add(slopeNightSpinner);

                claheDayNightPanel.add(p1);
                claheDayNightPanel.add(p2);

                JLabel claheLabel = new JLabel("Enhance Local Contrast: ");
                claheLabel.setPreferredSize(new Dimension(165, 20));

                JPanel clahePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
                clahePanel.add(claheLabel);
                clahePanel.add(blockRadiusSpinner);
                clahePanel.add(binsSpinner);
                clahePanel.add(slopeSpinner);
                clahePanel.add(slopeDayNightCheckBox);

                Box claheBox = Box.createVerticalBox();
                claheBox.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
                claheBox.add(clahePanel);
                claheBox.add(claheDayNightPanel);

                // --------------------------------------------------------

                final JTextField imagePathField = new JTextField(30);
                final JButton imagePathButton = new JButton("...");
                imagePathButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                inputDirPath = IJ.getDirectory("Choose the Images Directory");
                                if (inputDirPath != null) {
                                        File inputDirFile = new File(inputDirPath);

                                        if (inputDirFile.exists()) {
                                                imagePathField.setText(inputDirFile.getAbsolutePath());
                                        }
                                        else {
                                                IJ.showMessage("The images directory does not exist.");
                                        }
                                }
                                else {
                                        IJ.showMessage("An images directory must be selected.");
                                }
                        }
                });

                final JTextField metadataPathField = new JTextField(30);
                final JButton metadataPathButton = new JButton("...");

                metaDataCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                metaDataCheckBox.isSelected();
                                if (metaDataCheckBox.isSelected()) {
                                        metadataPathField.setEnabled(true);
                                        metadataPathButton.setEnabled(true);
                                        metadataPathButton.doClick();
                                }
                                else {
                                        metadataPathField.setEnabled(false);
                                        metadataPathButton.setEnabled(false);
                                }
                        }
                });

                JPanel miniPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
                miniPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                miniPanel.add(imagePathButton);
                miniPanel.add(metaDataCheckBox);

                JLabel imagesLabel = new JLabel("Images:");
                imagesLabel.setPreferredSize(new Dimension(60, 20));

                JPanel imageFilesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
                imageFilesPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                imageFilesPanel.add(imagesLabel);
                imageFilesPanel.add(imagePathField);
                imageFilesPanel.add(miniPanel);

                metadataPathField.setEnabled(false);
                metadataPathButton.setEnabled(false);
                metadataPathButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                metadataPath = IJ.getFilePath("Choose Metadata CSV File");
                                if (metadataPath != null) {
                                        File metadataFile = new File(metadataPath);

                                        if (metadataFile.exists()) {
                                                metadataPathField.setText(metadataFile.getAbsolutePath());
                                        }
                                        else {
                                                IJ.showMessage("The metaedata file does not exist.");
                                        }
                                }
                                else {
                                        IJ.showMessage("A metadata file must be selected.");
                                        metaDataCheckBox.setSelected(false);
                                }
                        }
                });

                JLabel metadataLabel = new JLabel("Metadata:");
                metadataLabel.setPreferredSize(new Dimension(60, 20));
                JPanel metadataPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
                metadataPanel.add(metadataLabel);
                metadataPanel.add(metadataPathField);
                metadataPanel.add(metadataPathButton);

                Box filesBox = Box.createVerticalBox();
                filesBox.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
                filesBox.add(imageFilesPanel);
                filesBox.add(Box.createVerticalStrut(5));
                filesBox.add(metadataPanel);

                // -------------------------------------------------------
                
                
                twoPassMinParticleSizeSlider.setLabel("Min Particle Size (px):");
                twoPassMaxParticleSizeSlider.setLabel("Max:", new Dimension(123, 20));
                twoPassMaxParticleSizeSlider.setShowInfinityOnMax(true);
                
                final JPanel twoPassParticleSizePanel = new JPanel(new GridLayout(2, 1, 4, 4));
                twoPassParticleSizePanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                twoPassParticleSizePanel.add(twoPassMinParticleSizeSlider);
                twoPassParticleSizePanel.add(twoPassMaxParticleSizeSlider);
                twoPassParticleSizePanel.setVisible(false);

                twoPassMinParticleCircSlider.setLabel("Min Particle Circularity:");
                twoPassMaxParticleCircSlider.setLabel("Max:", new Dimension(131, 20));

                final JPanel twoPassParticleCircPanel = new JPanel(new GridLayout(2, 1, 4, 4));
                twoPassParticleCircPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
                twoPassParticleCircPanel.add(twoPassMinParticleCircSlider);
                twoPassParticleCircPanel.add(twoPassMaxParticleCircSlider);
                twoPassParticleCircPanel.setVisible(false);
                
                twoPassCheckBox.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                if (((JCheckBox) e.getSource()).isSelected()) {
                                        twoPassParticleCircPanel.setVisible(true);
                                        twoPassParticleSizePanel.setVisible(true);
                                }
                                else {
                                        twoPassParticleCircPanel.setVisible(false);
                                        twoPassParticleSizePanel.setVisible(false);
                                }
                        }
                });

                JPanel twoPassBox = new JPanel(new GridLayout(3, 1, 5, 5));
                twoPassBox.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
                twoPassBox.add(twoPassCheckBox);
                twoPassBox.add(twoPassParticleSizePanel);
                twoPassBox.add(twoPassParticleCircPanel);

                // --------------------------------------------------------

                JPanel booleanBox = new JPanel(new GridLayout(2, 1, 4, 4));
                booleanBox.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
                booleanBox.add(saveSummaryCheckBox);
                booleanBox.add(Box.createVerticalStrut(5));
                booleanBox.add(saveOverlaysCheckBox);

                // --------------------------------------------------------

                Box mainBox = Box.createVerticalBox();
                mainBox.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                mainBox.add(subBackgroundBox);
                mainBox.add(Box.createVerticalStrut(5));
                mainBox.add(minParticleSizePanel);
                mainBox.add(Box.createVerticalStrut(5));
                mainBox.add(minParticleCircPanel);
                mainBox.add(Box.createVerticalStrut(5));
                mainBox.add(thresholdPanel);
                mainBox.add(Box.createVerticalStrut(5));
                mainBox.add(claheBox);
                mainBox.add(Box.createVerticalStrut(5));
                mainBox.add(filesBox);
                mainBox.add(Box.createVerticalStrut(5));
                mainBox.add(twoPassBox);
                mainBox.add(Box.createVerticalStrut(5));
                mainBox.add(booleanBox);

                // --------------------------------------------------------

                okButton = new JButton("OK");
                okButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                okButton_actionPerformed();
                        }
                });

                cancelButton = new JButton("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                                cancelButton_actionPerformed();
                        }
                });

                JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));
                buttonPanel.add(okButton);
                buttonPanel.add(cancelButton);

                // --------------------------------------------------------

                setTitle("Spotted Catalyst Options");
                getContentPane().add(mainBox, BorderLayout.NORTH);
                getContentPane().add(buttonPanel, BorderLayout.SOUTH);
                setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent e) {
                                cancelButton_actionPerformed();
                        }
                });

                pack();
                setModalityType(ModalityType.APPLICATION_MODAL);
                setVisible(true);
        }

        private void okButton_actionPerformed() {
                analysisOpt = new AnalysisOptions();
                analysisOpt.setRollingValue(((Double) subBackgroundSpinner.getValue()).floatValue());
                analysisOpt.setRollingDayValue(((Double) subBackgroundDaySpinner.getValue()).floatValue());
                analysisOpt.setRollingNightValue(((Double) subBackgroundNightSpinner.getValue()).floatValue());
                analysisOpt.setUseDayNightBackground(subBackgroundCheckBox.isSelected());
                analysisOpt.setRollingLightBackground(subBackgroundLightCheckBox.isSelected());
                analysisOpt.setMinParticleSize(minParticleSizeSlider.getValue());
                analysisOpt.setMaxParticleSize(maxParticleSizeSlider.getValue());
                analysisOpt.setMinParticleCirc(minParticleCircSlider.getValue());
                analysisOpt.setMaxParticleCirc(maxParticleCircSlider.getValue());
                analysisOpt.setThresholdMethod(thresholdComboBox.getSelectedItem().toString());
                analysisOpt.setThresholdDarkBackground(thresholdDarkBbgCheckBox.isSelected());
                analysisOpt.setClaheBlockRadius(((Integer) blockRadiusSpinner.getValue()).intValue());
                analysisOpt.setClaheBins(((Integer) binsSpinner.getValue()).intValue());
                analysisOpt.setClaheSlope(((Double) slopeSpinner.getValue()).floatValue());
                analysisOpt.setClaheDaySlope(((Double) slopeDaySpinner.getValue()).floatValue());
                analysisOpt.setClaheNightSlope(((Double) slopeNightSpinner.getValue()).floatValue());
                analysisOpt.setUseDayNightSlopes(slopeDayNightCheckBox.isSelected());
                analysisOpt.setPrintSummaries(saveSummaryCheckBox.isSelected());
                analysisOpt.setSaveOverlays(saveOverlaysCheckBox.isSelected());
                analysisOpt.setUseMetadata(metaDataCheckBox.isSelected());
                analysisOpt.setInputDirFile(new File(inputDirPath));
                analysisOpt.setTwoPassAnalysis(twoPassCheckBox.isSelected());
                analysisOpt.setTwoPassMinParticleSize(twoPassMinParticleSizeSlider.getValue());
                analysisOpt.setTwoPassMaxParticleSize(twoPassMaxParticleSizeSlider.getValue());
                analysisOpt.setTwoPassMinParticleCirc(twoPassMinParticleCircSlider.getValue());
                analysisOpt.setTwoPassMaxParticleCirc(twoPassMaxParticleCircSlider.getValue());

                if (metaDataCheckBox.isSelected()) {
                        analysisOpt.setMetadataFile(new File(metadataPath));
                }

                setVisible(false);
                dispose();
        }

        private void cancelButton_actionPerformed() {
                analysisOpt = null;
                setVisible(false);
                dispose();
        }

        public boolean hasAnalysisOptions() {
                return analysisOpt != null;
        }

        public AnalysisOptions getAnalysisOptions() {
                return analysisOpt;
        }
}
