package com.thomsonreuters.spottedcat.ui;
import java.io.File;

/**
 * 
 * @author Georges El-Haddad (georges.haddad@thomsonreuters.com)<br>
 * Created on: 12:12:16 PM - Aug 22, 2014
 */
public class AnalysisOptions {

        private double rollingValue = 0.0;
        private double rollingDayValue = 0.0;
        private double rollingNightValue = 0.0;
        private boolean rollingLightBackground = false;
        private double minParticleSize = 0.0;
        private double maxParticleSize = 0.0;
        private double minParticleCirc = 0.0;
        private double maxParticleCirc = 0.0;
        private String thresholdMethod = null;
        private boolean thresholdDarkBackground = false;
        private int claheBlockRadius = 0;
        private int claheBins = 0;
        private float claheSlope = 0.0f;
        private float claheDaySlope = 0.0f;
        private float claheNightSlope = 0.0f;
        private boolean saveOverlays = false;
        private boolean printSummaries = true;
        private boolean useMetadata = false;
        private boolean useDayNightSlopes = true;
        private boolean useDayNightBackground = true;
        private boolean twoPassAnalysis = false;
        private double twoPassMinParticleSize = 0.0;
        private double twoPassMaxParticleSize = 0.0;
        private double twoPassMinParticleCirc = 0.0;
        private double twoPassMaxParticleCirc = 0.0;
        private File inputDirFile = null;
        private File metadataFile = null;

        public AnalysisOptions() {

        }

        public final double getRollingValue() {
                return rollingValue;
        }

        public final void setRollingValue(double rollingValue) {
                this.rollingValue = rollingValue;
        }

        public final double getRollingDayValue() {
                return rollingDayValue;
        }

        public final void setRollingDayValue(double rollingDayValue) {
                this.rollingDayValue = rollingDayValue;
        }

        public final double getRollingNightValue() {
                return rollingNightValue;
        }

        public final void setRollingNightValue(double rollingNightValue) {
                this.rollingNightValue = rollingNightValue;
        }

        public final double getMinParticleSize() {
                return minParticleSize;
        }

        public final void setMinParticleSize(double minParticleSize) {
                this.minParticleSize = minParticleSize;
        }

        public final double getMinParticleCirc() {
                return minParticleCirc;
        }

        public final void setMinParticleCirc(double minParticleCirc) {
                this.minParticleCirc = minParticleCirc;
        }

        public final String getThresholdMethod() {
                return thresholdMethod;
        }

        public final void setThresholdMethod(String thresholdMethod) {
                this.thresholdMethod = thresholdMethod;
        }

        public final int getClaheBlockRadius() {
                return claheBlockRadius;
        }

        public final void setClaheBlockRadius(int claheBlockRadius) {
                this.claheBlockRadius = claheBlockRadius;
        }

        public final int getClaheBins() {
                return claheBins;
        }

        public final void setClaheBins(int claheBins) {
                this.claheBins = claheBins;
        }

        public final float getClaheSlope() {
                return claheSlope;
        }

        public final void setClaheSlope(float claheSlope) {
                this.claheSlope = claheSlope;
        }

        public final boolean isSaveOverlays() {
                return saveOverlays;
        }

        public final void setSaveOverlays(boolean saveOverlays) {
                this.saveOverlays = saveOverlays;
        }

        public final boolean isPrintSummaries() {
                return printSummaries;
        }

        public final void setPrintSummaries(boolean printSummaries) {
                this.printSummaries = printSummaries;
        }

        public final boolean isUseMetadata() {
                return useMetadata;
        }

        public final void setUseMetadata(boolean useMetadata) {
                this.useMetadata = useMetadata;
        }

        public final File getInputDirFile() {
                return inputDirFile;
        }

        public final void setInputDirFile(File inputDirFile) {
                this.inputDirFile = inputDirFile;
        }

        public final File getMetadataFile() {
                return metadataFile;
        }

        public final void setMetadataFile(File metadataFile) {
                this.metadataFile = metadataFile;
        }

        public final float getClaheDaySlope() {
                return claheDaySlope;
        }

        public final void setClaheDaySlope(float claheDaySlope) {
                this.claheDaySlope = claheDaySlope;
        }

        public final float getClaheNightSlope() {
                return claheNightSlope;
        }

        public final void setClaheNightSlope(float claheNightSlope) {
                this.claheNightSlope = claheNightSlope;
        }

        public final boolean isUseDayNightSlopes() {
                return useDayNightSlopes;
        }

        public final void setUseDayNightSlopes(boolean useDayNightSlopes) {
                this.useDayNightSlopes = useDayNightSlopes;
        }

        public final boolean isUseDayNightBackground() {
                return useDayNightBackground;
        }

        public final void setUseDayNightBackground(boolean useDayNightBackground) {
                this.useDayNightBackground = useDayNightBackground;
        }

        public final boolean isTwoPassAnalysis() {
                return twoPassAnalysis;
        }

        public final void setTwoPassAnalysis(boolean twoPassAnalysis) {
                this.twoPassAnalysis = twoPassAnalysis;
        }

        public final double getTwoPassMinParticleSize() {
                return twoPassMinParticleSize;
        }

        public final void setTwoPassMinParticleSize(double twoPassMinParticleSize) {
                this.twoPassMinParticleSize = twoPassMinParticleSize;
        }

        public final double getTwoPassMinParticleCirc() {
                return twoPassMinParticleCirc;
        }

        public final void setTwoPassMinParticleCirc(double twoPassMinParticleCirc) {
                this.twoPassMinParticleCirc = twoPassMinParticleCirc;
        }

        public final boolean isRollingLightBackground() {
                return rollingLightBackground;
        }

        public final void setRollingLightBackground(boolean rollingLightBackground) {
                this.rollingLightBackground = rollingLightBackground;
        }

        public final boolean isThresholdDarkBackground() {
                return thresholdDarkBackground;
        }

        public final void setThresholdDarkBackground(boolean thresholdDarkBackground) {
                this.thresholdDarkBackground = thresholdDarkBackground;
        }
        
        //The user should convert the max value to "Infinity" if set so
        public final double getMaxParticleSize() {
                return maxParticleSize;
        }

        public final void setMaxParticleSize(double maxParticleSize) {
                this.maxParticleSize = maxParticleSize;
        }

        public final double getMaxParticleCirc() {
                return maxParticleCirc;
        }

        public final void setMaxParticleCirc(double maxParticleCirc) {
                this.maxParticleCirc = maxParticleCirc;
        }

        public final double getTwoPassMaxParticleSize() {
                return twoPassMaxParticleSize;
        }

        public final void setTwoPassMaxParticleSize(double twoPassMaxParticleSize) {
                this.twoPassMaxParticleSize = twoPassMaxParticleSize;
        }

        public final double getTwoPassMaxParticleCirc() {
                return twoPassMaxParticleCirc;
        }

        public final void setTwoPassMaxParticleCirc(double twoPassMaxParticleCirc) {
                this.twoPassMaxParticleCirc = twoPassMaxParticleCirc;
        }
}
