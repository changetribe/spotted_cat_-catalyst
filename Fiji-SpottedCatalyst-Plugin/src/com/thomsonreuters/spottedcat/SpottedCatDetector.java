package com.thomsonreuters.spottedcat;
import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.PlugIn;
import ij.plugin.filter.BackgroundSubtracter;
import ij.plugin.frame.RoiManager;
import ij.process.ImageConverter;
import ij.text.TextPanel;
import ij.text.TextWindow;
import java.awt.Color;
import java.awt.Window;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.UIManager;
import com.thomsonreuters.spottedcat.plugins.RGBMeasurements;
import com.thomsonreuters.spottedcat.plugins.RGB_Measure_Plus;
import com.thomsonreuters.spottedcat.ui.AnalysisOptions;
import com.thomsonreuters.spottedcat.ui.AnalysisOptionsDialog;

/**
 * @author Georges El-Haddad (georges.haddad@thomsonreuters.com)<br>
 * Created on: August 8, 2014
 */
public class SpottedCatDetector implements PlugIn {
	
        private BufferedWriter summBw = null;
        private int globalCounter = 0;
        private boolean printHeadings = true;
        private AnalysisOptions analysisOpt = null;
        private int clearSummaries = 800;

        @Override
        public void run(String arg) {
                if (IJ.versionLessThan("1.49f")) {
                        IJ.showMessage("This plugin needs to run on ImageJ v1.49f and above");
                }
                else {
                        try {
                                UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
                        }
                        catch (Exception e) {
                                //Will use the standard Look&Feel
                        }
                        
                        AnalysisOptionsDialog analysisDialog = new AnalysisOptionsDialog();
                        analysisOpt = analysisDialog.getAnalysisOptions();
                        analysisDialog = null;

                        if (analysisOpt != null) {
                                long start = System.currentTimeMillis();

                                try {
                                        openStreams();
                                        batchAnalysis();
                                }
                                catch (IOException ioe) {
                                        ioe.printStackTrace();
                                        IJ.error("Plugin Error", "There was an error initiating the file I/O streams: " + ioe.getMessage());
                                }

                                closeStreams();

                                long stop = System.currentTimeMillis();

                                System.gc();

                                Window window = WindowManager.getWindow("Summary");
                                window.setVisible(false);
                                TextWindow txtWin = (TextWindow) window;
                                txtWin.getTextPanel().clear();
                                txtWin.close();
                                txtWin = null;
                                window.dispose();

                                RoiManager roiManager = RoiManager.getInstance();
                                if (roiManager != null) {
                                        roiManager.close();
                                        roiManager.dispose();
                                }

                                analysisOpt = null;

                                IJ.showMessage("Done. The analysis took " + ((stop - start) / 1000) + " s");
                        }
                        else {
                                IJ.showMessage("Analysis options must be selected in order to continue");
                        }
                }
        }
        
        /**
         * Opens the input/output stream to the summaries file.
         * 
         * @throws IOException on error when creating the streams
         */
        private final void openStreams() throws IOException {
                if (analysisOpt.isPrintSummaries()) {
                        summBw = new BufferedWriter(new FileWriter(analysisOpt.getInputDirFile().getAbsolutePath() + File.separator + "results.xls"));
                }
        }
        
        /**
         * Closes the input/output streams and supresses any errors
         * that may be caused during this operation.
         */
        private final void closeStreams() {
                if (summBw != null) {
                        try {
                                summBw.close();
                        }
                        catch (Exception ex) {}
                        summBw = null;
                }
        }
        
        /**
         * Performs the batch analysis of given set of images.
         * Files are loaded, either with or without a corresponding
         * metadata file. Initial measurements are set and then the
         * main loop is started to analyze the images.
         */
        private final void batchAnalysis() {
                ImageDoc[] inputList = null;

                if (analysisOpt.isUseMetadata()) {
                        try {
                                inputList = loadFilesFromMetadata();
                        }
                        catch (IOException ioe) {
                                IJ.error("I/O Error", "There was an error reading the metadata file: " + ioe.getMessage());
                                ioe.printStackTrace();
                                return;
                        }
                }
                else {
                        inputList = loadFiles();
                }

                ImageDoc imageDoc = null;
                String baseDir = analysisOpt.getInputDirFile().getAbsolutePath();

                IJ.run("Set Measurements...", "area mean standard modal min centroid center perimeter bounding fit shape feret's integrated median area_fraction redirect=None decimal=3");
                
                for (int i = 0; i < inputList.length; i++) {
                        imageDoc = inputList[i];
                        Results[] results = analyzeImage(baseDir, imageDoc);
                        if (results != null) {
                                if (analysisOpt.isPrintSummaries()) {
                                        try {
                                                printSummaries(results);
                                        }
                                        catch (Exception ex) {
                                                ex.printStackTrace();
                                        }
                                        finally {
                                                results = null;
                                        }
                                }
                        }
                }

                imageDoc = null;
                baseDir = null;
                inputList = null;
        }
        
        /**
         * Analyzes a single image by running through a series of 
         * pre-processing steps and then running it through a particle
         * analyzer. User defined options are retrieved here.
         * <p>
         * Note that this method will automatically focus on, extract and
         * clean up the "Summary" window that ImageJ pops up during the
         * particle analysis. Currently there is no other way to gather
         * the summary. Cleaning the contents of the summary window is
         * imperative to performance of the application.
         * 
         * @param baseDir - the base directory where the images reside
         * @param imageDoc - the object representing a single image to be analyzed
         * @return an array of results. This may be 1 result if the single pass option was
         *         selected or 2 results if the two pass option was selected
         */
        private final Results[] analyzeImage(String baseDir, ImageDoc imageDoc) {
                Results[] resultsArr = null;
                ImagePlus image = null;
                ImagePlus imageDup = null;
                
                try {
                        image = IJ.openImage(baseDir + File.separator + imageDoc.getImageName());
                        imageDup = image.duplicate();
                }
                catch (Exception ex) {
                        IJ.log("Skipping corrupted image: " + imageDoc.getImageName());
                        return null;
                }
                
                float slope;
                if (analysisOpt.isUseDayNightSlopes()) {
                        if (imageDoc.isDay()) {
                                slope = analysisOpt.getClaheDaySlope();
                        }
                        else {
                                slope = analysisOpt.getClaheNightSlope();
                        }
                }
                else {
                        slope = analysisOpt.getClaheSlope();
                }

                try {
                        //Catches corrupt images and ignores them.
                        mpicbg.ij.clahe.Flat.getFastInstance().run(
                                                        image,
                                                        analysisOpt.getClaheBlockRadius(),
                                                        analysisOpt.getClaheBins(),
                                                        slope,
                                                        null,       // mask
                                                        false       // composite
                                                       );
                }
                catch (Exception ex) {
                        return null;
                }

                image.updateImage();
                ImageConverter ic = new ImageConverter(image);
                ic.convertToGray8();
                image.updateImage();

                double rollingValue;
                if (analysisOpt.isUseDayNightBackground()) {
                        if (imageDoc.isDay()) {
                                rollingValue = analysisOpt.getRollingDayValue();
                        }
                        else {
                                rollingValue = analysisOpt.getRollingNightValue();
                        }
                }
                else {
                        rollingValue = analysisOpt.getRollingDayValue();
                }
                
                boolean isLightBackground = analysisOpt.isRollingLightBackground();
                
                BackgroundSubtracter bs = new BackgroundSubtracter();
                bs.rollingBallBackground(image.getChannelProcessor(), rollingValue, false, isLightBackground, false, false, false);
                image.updateImage();
                
                String thresholdOptions = analysisOpt.getThresholdMethod();
                if(analysisOpt.isThresholdDarkBackground()) {
                        thresholdOptions = thresholdOptions+" dark";
                }
                
                IJ.setAutoThreshold(image, thresholdOptions);
                image.updateImage();
                
                //Prefs.blackBackground = false;
                IJ.run(image, "Convert to Mask", "");
                image.updateImage();

                if (analysisOpt.isTwoPassAnalysis()) {
                        resultsArr = new Results[2];
                }
                else {
                        resultsArr = new Results[1];
                }
                
                //Perform a cleanup of the summary window every clearSummaries time.
                //This will cause a small visual anomaly of a window apearing and then
                //disapearing very quickly. This is done because calling the method
                //to delete all the lines while the window is still visible does not
                //work. So the window has to physically close and disposed of in memory.
                //A garabage collection is then hinted at to keep memory consumption low.
                if (globalCounter >= clearSummaries) {
                        globalCounter = 0;
                        Window window = WindowManager.getWindow("Summary");
                        window.setVisible(false);
                        TextWindow txtWin = (TextWindow) window;
                        txtWin.getTextPanel().clear();
                        txtWin.close();
                        txtWin = null;
                        window.dispose();
                        System.gc();
                }
                
                double minParticleSize = analysisOpt.getMinParticleSize();
                double maxParticleSize = analysisOpt.getMaxParticleSize();
                double minParticleCirc = analysisOpt.getMinParticleCirc();
                double maxParticleCirc = analysisOpt.getMaxParticleCirc();

                Results results = analyzeParticles(imageDoc, image, minParticleSize, maxParticleSize, minParticleCirc, maxParticleCirc, 1);
                String colorResults = processColorValues(imageDup);
                if(colorResults != null) {
                        results.appendResults(colorResults, "R-Mean\tG-Mean\tB-Mean\tR-Std.Dev\tG-Std.Dev\tB-Std.Dev");
                }
                else {
                        results.appendResults("NaN\tNaN\tNaN\tNaN\tNaN\tNaN\t", "R-Mean\tG-Mean\tB-Mean\tR-Std.Dev\tG-Std.Dev\tB-Std.Dev");
                }
                
                resultsArr[0] = results;
                globalCounter++;
                
                if (analysisOpt.isTwoPassAnalysis()) {
                        minParticleSize = analysisOpt.getTwoPassMinParticleSize();
                        maxParticleSize = analysisOpt.getTwoPassMaxParticleSize();
                        minParticleCirc = analysisOpt.getTwoPassMinParticleCirc();
                        maxParticleCirc = analysisOpt.getTwoPassMaxParticleCirc();
                        
                        Results results2 = analyzeParticles(imageDoc, image, minParticleSize, maxParticleSize, minParticleCirc, maxParticleCirc, 2);
                        if(colorResults != null) {
                                results2.appendResults(colorResults, "R-Mean\tG-Mean\tB-Mean\tR-Std.Dev\tG-Std.Dev\tB-Std.Dev");
                        }
                        else {
                                results2.appendResults("NaN\tNaN\tNaN\tNaN\tNaN\tNaN\t", "R-Mean\tG-Mean\tB-Mean\tR-Std.Dev\tG-Std.Dev\tB-Std.Dev");
                        }
                        
                        resultsArr[1] = results2;
                        globalCounter++;
                }
                
                thresholdOptions = null;
                image.unlock();
                image.flush();
                image = null;
                imageDup.unlock();
                imageDup.flush();
                imageDup = null;
                ic = null;
                bs = null;

                return resultsArr;
        }

        /**
         * Performs the particle analysis on the given image with user defined
         * minimum particle size and minimum particle circularity values. The number
         * of passes is also sent in case the user wants to save the overlay outlines
         * of the particles for each image.
         * <p>
         * A results object is returned with a summary of the results.
         * 
         * @param imageDoc - the object representing a single image to be analyzed
         * @param image - the image to be analyzed
         * @param minParticleSize - the minimum particle size
         * @param minParticleCircularity - the minimum particle circularity
         * @param maxParticleSize - the maximum particle size
         * @param maxParticleCircularity - the maximum particle circularity
         * @param passNumber - the pass number 
         * @return a results object with the list of summary results
         */
        private final Results analyzeParticles(ImageDoc imageDoc, ImagePlus image, double minParticleSize, double maxParticleSize, double minParticleCircularity, double maxParticleCircularity, int passNumber) {
                StringBuilder options = new StringBuilder();
                options.append("size=");
                options.append(minParticleSize);
                options.append("-");
                if(maxParticleSize == 1.00) {
                        options.append("Infinity");
                }
                else {
                        options.append(maxParticleSize);
                }
                
                options.append(" circularity=");
                options.append(minParticleCircularity);
                options.append("-");
                options.append(maxParticleCircularity);
                options.append(" exclude clear summarize add");
                options.append(" add");
                
                IJ.run(image, "Analyze Particles...", options.toString());
                options = null;

                Results results = new Results();
                Window window = WindowManager.getWindow("Summary");
                if (window != null) {
                        window.setVisible(false);
                        TextWindow txtWindow = (TextWindow) window;
                        TextPanel txtPanel = txtWindow.getTextPanel();
                        String strSummary = txtPanel.getLine(globalCounter);
                        if (strSummary != null && !strSummary.isEmpty()) {
                                results.setSummaryHeadings(txtPanel.getColumnHeadings());
                                results.setSummaryLine(strSummary);
                        }
                }
                
                if (analysisOpt.isSaveOverlays()) {
                        saveOverlay(imageDoc, image, passNumber);
                }

                return results;
        }
        
        private final String processColorValues(ImagePlus imageDup) {
                String colorResults = null;
                
                RoiManager roiManager = RoiManager.getInstance();
                if (roiManager.isVisible()) {
                        roiManager.setVisible(false);
                }

                Roi[] rois = roiManager.getRoisAsArray();
                for (int i = 0; i < rois.length; i++) {
                        Roi roi = rois[i];
                        roi.setStrokeColor(Color.MAGENTA);
                        roi.setStrokeWidth(1);
                        roi.setPosition(i + 1);
                        imageDup.setRoi(roi);
                }

                imageDup.updateImage();

                RGB_Measure_Plus rgbMeasurePlus = new RGB_Measure_Plus();
                RGBMeasurements rgbMeasurments = null;
                try {
                        rgbMeasurments = rgbMeasurePlus.measureRGB(imageDup);
                        StringBuilder sb = new StringBuilder();
                        sb.append(rgbMeasurments.getrMean())
                        .append("\t")
                        .append(rgbMeasurments.getgMean())
                        .append("\t")
                        .append(rgbMeasurments.getbMean())
                        .append("\t")
                        .append(rgbMeasurments.getrStdDev())
                        .append("\t")
                        .append(rgbMeasurments.getgStdDev())
                        .append("\t")
                        .append(rgbMeasurments.getbStdDev());
                        colorResults = sb.toString();
                        sb = null;
                        rgbMeasurments = null;
                }
                catch(Exception ex) {
                        ex.printStackTrace();
                }
                
                rgbMeasurePlus = null;
                
                return colorResults;
        }
        
        /**
         * Saves the overlay outlines of the detected particles for the
         * specified image. This procedure will acquire the ROI (Region of Interest)
         * manager window of the current image being analyzed and extract
         * the ROI list and create an overlay image. This overlay is then
         * drawn ontop of a copy of the pre-processed image and then saved
         * with a "_outlines_x.jpg" suffix.
         * 
         * @param imageDoc - the object representing a single image to be analyzed
         * @param image - the image to be analyzed
         * @param passNumber - the current pass number
         */
        private final void saveOverlay(ImageDoc imageDoc, ImagePlus image, int passNumber) {
                RoiManager roiManager = RoiManager.getInstance();
                if (roiManager.isVisible()) {
                        roiManager.setVisible(false);
                }
                
                Roi[] rois = roiManager.getRoisAsArray();
                Overlay over = new Overlay();
                for (int i = 0; i < rois.length; i++) {
                        Roi roi = rois[i];
                        roi.setStrokeColor(Color.MAGENTA);
                        roi.setStrokeWidth(1.5);
                        over.add(roi);
                }

                ImagePlus imp = image.duplicate();
                imp.setOverlay(over);
                imp.updateAndDraw();

                String outlineName = imageDoc.getImageName().replace(".jpg", "_outlines_" + passNumber + ".jpg");
                IJ.saveAs(imp, "jpg", analysisOpt.getInputDirFile().getAbsolutePath() + File.separator + outlineName);

                imp.flush();
                outlineName = null;
                rois = null;
                imp = null;
        }
        
        /**
         * Loads image files from the given directory and returns
         * an array of image document objects. The night/day flag
         * is set to false here because no metadata file has been
         * specified.
         * 
         * @return array of ImageDoc objects where the night/day flag
         *         is set to false due to no metadata file being specified
         */
        private final ImageDoc[] loadFiles() {
                String[] inputList = analysisOpt.getInputDirFile().list();
                ArrayList<ImageDoc> tmpList = new ArrayList<ImageDoc>();
                for (int i = 0; i < inputList.length; i++) {
                        if(!inputList[i].toLowerCase().endsWith(".xls")) {
                                tmpList.add(new ImageDoc(inputList[i], false));
                        }
                }
                
                ImageDoc[] arr = new ImageDoc[tmpList.size()];
                arr = tmpList.toArray(arr);
                
                inputList = null;
                tmpList = null;
                return arr;
        }
        
        /**
         * Loads all files from the given metadata file including the
         * night/day flag.
         * 
         * @return array of ImageDoc objects where night/day flag is properly set
         * @throws IOException if there is an error reading the metadata file
         */
        private final ImageDoc[] loadFilesFromMetadata() throws IOException {
                ImageDoc[] arr = null;
                ArrayList<ImageDoc> list = new ArrayList<ImageDoc>();
                BufferedReader bw = null;

                try {
                        bw = new BufferedReader(new FileReader(analysisOpt.getMetadataFile()));
                        String line = null;

                        // Skip the first line as it is just the header
                        bw.readLine();
                        while ((line = bw.readLine()) != null) {
                                String[] spliced = line.split(",");
                                list.add(new ImageDoc(spliced[0], spliced[1]));
                        }
                }
                finally {
                        try {
                                bw.close();
                                bw = null;
                        }
                        catch (Exception ex) {};
                }

                arr = new ImageDoc[list.size()];
                arr = list.toArray(arr);
                list = null;
                return arr;
        }
        
        /**
         * Constructs the summary to be written in the
         * the results file. The results file is flushed after every write.
         * 
         * @param results - the results to write to the results file
         * @throws IOException if an error occurs while writing to the file
         */
        private final void printSummaries(Results[] results) throws IOException {
                if (analysisOpt.isTwoPassAnalysis()) {
                        if (results.length == 2) {
                                if (printHeadings) {
                                        printSummaryHeadings(results[0]);
                                        printHeadings = false;
                                }

                                String[] summaryLine1 = results[0].getSummaryLine();
                                String[] summaryLine2 = results[1].getSummaryLine();
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < summaryLine1.length; i++) {
                                        sb.append(summaryLine1[i]);
                                        sb.append('\t');
                                }

                                for (int i = 0; i < summaryLine2.length; i++) {
                                        sb.append(summaryLine2[i]);
                                        sb.append('\t');
                                }

                                sb.delete(sb.length() - 1, sb.length());

                                summBw.write(sb.toString());
                                summBw.write(System.getProperty("line.separator"));
                                summBw.flush();
                                sb = null;
                        }
                }
                else {
                        if (results.length == 1) {
                                if (printHeadings) {
                                        printSummayHeadingsTwoPass(results[0]);
                                        printHeadings = false;
                                }

                                String[] summaryLine = results[0].getSummaryLine();
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < summaryLine.length; i++) {
                                        sb.append(summaryLine[i]);
                                        sb.append('\t');
                                }

                                sb.delete(sb.length() - 1, sb.length());

                                summBw.write(sb.toString());
                                summBw.write(System.getProperty("line.separator"));
                                summBw.flush();
                                sb = null;
                        }
                }
        }
        
        /**
         * Prints the summary headings for the results file. This method
         * should be called once before writing the summaries file for
         * the entire result set.
         * 
         * @param results - the results to write to the results file
         * @throws IOException if there is an error writing the headings to the results file
         */
        private final void printSummaryHeadings(Results result) throws IOException {
                String[] summaryHeadings = result.getSummaryHeadings();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < summaryHeadings.length; i++) {
                        sb.append(summaryHeadings[i]);
                        sb.append('\t');
                }

                for (int i = 0; i < summaryHeadings.length; i++) {
                        sb.append(summaryHeadings[i]).append('2');
                        sb.append('\t');
                }

                sb.delete(sb.length() - 1, sb.length());

                summBw.write(sb.toString());
                summBw.write(System.getProperty("line.separator"));
                sb = null;
        }
        
        /**
         * Prints the summary headings for the 2 pass options to the results file.
         * This method should be called once before writing the summaries file for
         * the entire result set.
         * 
         * @param results - the results to write to the results file
         * @throws IOException if there is an error writing the headings to the results file
         */
        private final void printSummayHeadingsTwoPass(Results result) throws IOException {
                String[] summaryHeadings = result.getSummaryHeadings();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < summaryHeadings.length; i++) {
                        sb.append(summaryHeadings[i]);
                        sb.append('\t');
                }

                sb.delete(sb.length() - 1, sb.length());

                summBw.write(sb.toString());
                summBw.write(System.getProperty("line.separator"));
                sb = null;
        }
}
