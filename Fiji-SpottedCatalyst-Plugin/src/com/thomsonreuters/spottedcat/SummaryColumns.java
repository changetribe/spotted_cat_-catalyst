package com.thomsonreuters.spottedcat;

/**
 * 
 * @author Georges El-Haddad (georges.haddad@thomsonreuters.com)<br>
 * Created on: 3:33:42 PM - Aug 23, 2014
 */
public final class SummaryColumns {

        public static final int SCLICE = 0;
        public static final int COUNT = 1;
        public static final int TOTAL_AREA = 2;
        public static final int AVERAGE_SIZE = 3;
        public static final int PCT_AREA = 4;
        public static final int MEAN = 5;
        public static final int MODE = 6;
        public static final int PERIM = 7;
        public static final int MAJOR = 8;
        public static final int MINOR = 9;
        public static final int ANGLE = 10;
        public static final int CIRCULARITY = 11;
        public static final int SOLIDITY = 12;
        public static final int FERET = 13;
        public static final int FERET_X = 14;
        public static final int FERET_Y = 15;
        public static final int FERET_ANGLE = 16;
        public static final int MIN_FERET = 17;
        public static final int INTEGRATED_DENSITY = 18;
        public static final int MEDIAN = 19;
        
        private SummaryColumns() {
                
        }
}
