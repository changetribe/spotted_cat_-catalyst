package com.thomsonreuters.spottedcat;
/**
 * 
 * @author Georges El-Haddad (georges.haddad@thomsonreuters.com)<br>
 * Created on: 12:12:44 PM - Aug 22, 2014
 */
public class ImageDoc {

        private String imageName = null;
        private boolean daytime = true;
        
        public ImageDoc(String imageName, boolean daytime) {
                setImageName(imageName);
                setDaytime(daytime);
        }

        public ImageDoc(String imageName, String daytime) {
                setImageName(imageName);
                if (daytime.equals("0")) {
                        setDaytime(false);
                }
                else {
                        setDaytime(true);
                }
        }

        public void setImageName(String imageName) {
                if (imageName != null) {
                        this.imageName = imageName.toLowerCase();
                }
        }

        public void setDaytime(boolean isDay) {
                this.daytime = isDay;
        }

        public boolean isDay() {
                return daytime;
        }

        public String getImageName() {
                return imageName;
        }
}
