package com.thomsonreuters.spottedcat;

/**
 * 
 * @author Georges El-Haddad (georges.haddad@thomsonreuters.com)<br>
 * Created on: 12:12:35 PM - Aug 22, 2014
 */
public class Results {

        private String imageName = null;
        private String[] summaryHeadings = null;
        private String[] summaryLine = null;
        private boolean cat = false;

        public Results() {

        }
        
        public void appendResults(String results, String resultsHeading) {
                String[] xtraResults = results.split("\t");
                String[] xtraHeadings = resultsHeading.split("\t");
                
                String[] tmp1 = new String[summaryLine.length+xtraResults.length];
                String[] tmp2 = new String[summaryHeadings.length+xtraHeadings.length];
                
                int j = 0;
                for(int i=0; i < summaryLine.length; i++) {
                        tmp1[j] = summaryLine[i];
                        j++;
                }
                
                for(int i=0; i < xtraResults.length; i++) {
                        tmp1[j] = xtraResults[i];
                        j++;
                }
                
                //--- headings
                
                j = 0;
                for(int i=0; i < summaryHeadings.length; i++) {
                        tmp2[j] = summaryHeadings[i];
                        j++;
                }
                
                for(int i=0; i < xtraHeadings.length; i++) {
                        tmp2[j] = xtraHeadings[i];
                        j++;
                }
                
                summaryLine = tmp1;
                summaryHeadings = tmp2;
        }
        
        
        public void setImageName(String imageName) {
                this.imageName = imageName;
        }

        public void setSummaryLine(String summaryLine) {
                if (summaryLine != null) {
                        this.summaryLine = summaryLine.split("\t");
                }
                else {
                        this.summaryLine = null;
                }
        }

        public void setSummaryLine(String[] summaryLine) {
                this.summaryLine = summaryLine;
        }

        public void setSummaryHeadings(String summaryHeadings) {
                if (summaryHeadings != null) {
                        this.summaryHeadings = summaryHeadings.split("\t");
                }
                else {
                        this.summaryHeadings = null;
                }
        }

        public void setSummaryHeadings(String[] summaryHeadings) {
                this.summaryHeadings = summaryHeadings;
        }

        public String getImageName() {
                return imageName;
        }

        public String[] getSummaryLine() {
                return summaryLine;
        }

        public String[] getSummaryHeadings() {
                return summaryHeadings;
        }

        public boolean hasSummary() {
                return summaryLine != null && summaryLine.length > 0;
        }

        public String getSummaryCell(int column) {
                return summaryLine[column];
        }

        public double getSummaryCellDouble(int column) {
                if (SummaryColumns.SCLICE == column) {
                        // SCLICE is a text value so we just return 0.0
                        return 0.0;
                }
                else {
                        return Double.parseDouble(summaryLine[column]);
                }
        }

        public final boolean isCat() {
                return cat;
        }

        public final void setCat(boolean cat) {
                this.cat = cat;
        }
}
